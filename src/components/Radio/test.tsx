import { screen } from '@testing-library/react'
import { renderWithTheme } from 'utils/tests/helper'

import Radio from '.'

describe('<Radio />', () => {
  it('should render with label', () => {
    renderWithTheme(<Radio />)

    expect(screen.queryByLabelText('Radio')).not.toBeInTheDocument()
  })
})
