import { ThemeProvider } from 'styled-components'

import GlobalStyles from '../src/styles/globals'
import theme from '../src/styles/theme'

export const parameters = {
  backgrounds: {
    default: 'lightGray',
    values: [
      {
        name: 'lightGray',
        value: theme.colors.lightGray
      },
      {
        name: 'secondary',
        value: theme.colors.secondary
      }
    ]
  }
}

export const decorators = [
  (Story) => (
    <ThemeProvider theme={theme}>
      <GlobalStyles />
      <Story />
    </ThemeProvider>
  )
]
